import {Mapper} from "./mapper";
import {BookChange, BookPriceLevel, Exchange, Trade} from "../types";
import {lowerCaseSymbols} from "../handy";

export class HyperLiquidTradesMapper implements Mapper<'hyperliquid', Trade> {
    constructor(private readonly _exchange: Exchange) {}

    canHandle(message: HyperLiquidResponse<any>) {
        return message.channel == 'trades'
    }

    getFilters(symbols?: string[]) {
        if (!symbols) return []

        return symbols.map(s => {
            return {
                channel: 'trades',
                symbols: [s],
            }
        })
    }

    *map(tradeResponse: HyperLiquidResponse<Array<HyperLiquidTradeData>>, localTimestamp: Date) {
        const tradeData = tradeResponse.data

        for (const t of tradeData) {
            const trade: Trade = {
                type: 'trade',
                symbol: t.coin,
                exchange: this._exchange,
                id: t.hash,
                price: Number(t.px),
                amount: Number(t.sz),
                side: t.side == 'A' ? 'sell': 'buy',
                timestamp: new Date(t.time),
                localTimestamp: localTimestamp,
            }

            yield trade
        }
    }
}

export class HyperLiquidBookChangeMapper implements Mapper<'hyperliquid', BookChange> {
    constructor(protected readonly exchange: Exchange, protected readonly ignoreBookSnapshotOverlapError: boolean) {}

    canHandle(message: HyperLiquidResponse<any>) {
        return message.channel == 'l2Book'
    }

    getFilters(symbols?: string[]) {
        if (!symbols) return []

        return symbols.map(s => {
            return {
                channel: 'l2Book',
                symbols: [s],
            }
        })
    }

    *map(tradeResponse: HyperLiquidResponse<HyperLiquidDepthData>, localTimestamp: Date) {
        const data = tradeResponse.data

        const bookChange: BookChange = {
            type: 'book_change',
            symbol: data.coin,
            exchange: this.exchange,
            isSnapshot: true,
            bids: data.levels[0].map(this.mapBookLevel),
            asks: data.levels[1].map(this.mapBookLevel),
            timestamp: new Date(data.time),
            localTimestamp,
        }

        yield bookChange
    }

    protected mapBookLevel(level: HyperLiquidBookLevel) {
        const price = Number(level.px)
        const amount = Number(level.sz)

        return { price, amount }
    }
}

type HyperLiquidResponse<T> = {
    channel: string
    data: T
}

type HyperLiquidTradeData = {
    coin: string
    side: 'A' | 'B'
    px: string
    sz: string
    time: number
    hash: string
}

type HyperLiquidBookLevel = {
    px: string
    sz: string
    n: number
}

type HyperLiquidDepthData = {
    coin: string
    levels: [Array<HyperLiquidBookLevel>, Array<HyperLiquidBookLevel>]
    time: number
}