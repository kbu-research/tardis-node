import {MultiConnectionRealTimeFeedBase, RealTimeFeedBase} from "./realtimefeed";
import {Filter} from "../types";

export class HyperLiquidRealTimeFeed extends MultiConnectionRealTimeFeedBase {
    protected wssURL = 'wss://api.hyperliquid.xyz/ws';
    protected httpURL: string = 'https://api.hyperliquid.xyz';
    protected suffixes: { [key: string]: string } = {
        depth: '100ms'
    };
    protected depthRequestRequestWeight: number = 100;

    protected *_getRealTimeFeeds(exchange: string, filters: Filter<string>[], timeoutIntervalMS?: number, onError?: (error: Error) => void) {
        const wsFilters = filters.filter((f) => f.channel !== 'openInterest' && f.channel !== 'recentTrades')

        if (wsFilters.length > 0) {
            yield new HyperLiquidSingleConnectionRealTimeFeed(
                exchange,
                wsFilters,
                this.wssURL,
                this.httpURL,
                this.suffixes,
                this.depthRequestRequestWeight,
                timeoutIntervalMS,
                onError
            )
        }

        // const openInterestFilters = filters.filter((f) => f.channel === 'openInterest')

        // if (openInterestFilters.length > 0) {
        //     const instruments = openInterestFilters.flatMap((s) => s.symbols!)
        //
        //     yield new HyperLiquidOpenInterestClient(exchange, this.httpURL, instruments)
        // }
    }
}

// class HyperLiquidOpenInterestClient extends PoolingClientBase {
//     constructor(exchange: string, private readonly _httpURL: string, private readonly _instruments: string[]) {
//         super(exchange, 30)
//     }
//
//     protected async poolDataToStream(outputStream: Writable) {
//         for (const instruments of batch(this._instruments, 10)) {
//             await Promise.allSettled(
//                 instruments.map(async (instrument) => {
//                     if (outputStream.destroyed) {
//                         return
//                     }
//                     const openInterestResponse = (await httpClient
//                         .get(`${this._httpURL}/openInterest?symbol=${instrument.toUpperCase()}`, {
//                             timeout: 10 * 1000,
//                             retry: {
//                                 limit: 10,
//                                 statusCodes: [418, 429, 500, 403],
//                                 maxRetryAfter: 120 * 1000
//                             }
//                         })
//                         .json()) as any
//
//                     const openInterestMessage = {
//                         stream: `${instrument.toLocaleLowerCase()}@openInterest`,
//                         generated: true,
//                         data: openInterestResponse
//                     }
//
//                     if (outputStream.writable) {
//                         outputStream.write(openInterestMessage)
//                     }
//                 })
//             )
//         }
//     }
// }

class HyperLiquidSingleConnectionRealTimeFeed extends RealTimeFeedBase {
    constructor(
        exchange: string,
        filters: Filter<string>[],
        protected wssURL: string,
        private readonly _httpURL: string,
        private readonly _suffixes: { [key: string]: string },
        private readonly _depthRequestRequestWeight: number,
        timeoutIntervalMS: number | undefined,
        onError?: (error: Error) => void
    ) {
        super(exchange, filters, timeoutIntervalMS, onError)
    }

    protected mapToSubscribeMessages(filters: Filter<string>[]): any[] {
        return filters
            .map((filter, _) => {
                if (!filter.symbols || filter.symbols.length === 0) {
                    throw new Error('HyperLiquidRealTimeFeed requires explicitly specified symbols when subscribing to live feed')
                }

                return {
                    method: 'subscribe',
                    subscription: {"type": filter.channel, "coin": filter.symbols[0]}
                }
            })
    }

    protected messageIsError(_: any): boolean {
        return false;
    }
}
